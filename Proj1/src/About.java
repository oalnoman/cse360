/*
 * This class is used to create the GUI for the "About" Window and display information about
 * the project and the team.
 * 
 */



import java.awt.Color;
import javax.swing.*;

public class About extends JFrame {

	//Variables
	private JLabel jLabel1;
	private JScrollPane jScrollPane1;
	private JTextArea jTextArea1;
	private JLabel jLabel2;
	private static final long serialVersionUID = 1L;
	
	//constructor
    public About() {
        initComponents(); //call to initialize the variables
    }

                            
    private void initComponents() {

        jLabel1 = new JLabel();
        jScrollPane1 = new JScrollPane();
        jTextArea1 = new JTextArea();
        jLabel2 = new JLabel();
        
        //set default settings for the JFrame
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        getContentPane().setBackground(Color.WHITE);
        setTitle("About");
        setResizable(false);
        

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 51, 255));
        jLabel1.setText("About Activity Analyzer v1.0");
        
        jLabel2.setFont(new java.awt.Font("Lucida Grande", 1, 14));
        jLabel2.setText("Created By:\t\tOmar Al Noman,\t\tMaxwell Buss,\t\tAlekhya Hari");
        
        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("Lucida Grande", 0, 15)); // NOI18N
        jTextArea1.setLineWrap(true);
        jTextArea1.setRows(5);
        jTextArea1.setText("\nThe goal of this program is to create an activity diagram based on user input. The user will input all data for the network; activity, duration and dependencies. Once all node are inserted we will have a full network diagram. The user will then be able to receive data about the network of each possible path to be taken and the corresponding duration. Afterwards the user can decide to either create a new activity network or to quit the program.\n\n");
        jTextArea1.setWrapStyleWord(true);
        jTextArea1.setEditable(false);
        jScrollPane1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        jScrollPane1.setViewportView(jTextArea1);

        //Group Layout
        
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
            .addGap(0, 0, Short.MAX_VALUE)
            .addComponent(jLabel1)
            .addGap(141, 141, 141))
            .addGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 590, GroupLayout.PREFERRED_SIZE)
            .addComponent(jLabel2))
            .addGap(0, 23, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addGap(25, 25, 25)
            .addComponent(jLabel1)
            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 163, GroupLayout.PREFERRED_SIZE)
            .addGap(18, 18, 18)
            .addComponent(jLabel2)
            .addContainerGap(23, Short.MAX_VALUE))
        );

        pack();
        
        //Display the window in the middle of the screen
        setLocationRelativeTo(null);
    }                 
}
