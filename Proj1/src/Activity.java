/*
 * This class is used to create an activity and initialize the data.
 */


import java.util.LinkedList;


public class Activity implements Comparable<Activity>
{
	//variables
	protected String activityName;
	protected int duration;
	protected LinkedList<Activity> predecessors;
	protected LinkedList<String> predecessorStr;
	protected LinkedList<Activity> successors;
	
	
	//these two boolean variables are used in the traversal and detecting cycles methods
	protected boolean visited;
	protected boolean recStack;
	

	//Constructor
	public Activity(String activityName, int duration, LinkedList<String> pred)
	{
		this.activityName = activityName;
		this.duration = duration;
		this.predecessorStr= new LinkedList<String>(pred);
		this.predecessors = new LinkedList<>();
		this.successors = new LinkedList<>();
		this.visited = false;
		this.recStack = false;
	}
    
	//Methods
	// Link predecessors to the activity
	public void LinkPredecessors(LinkedList<Activity> activityList){
		for(String predecessor : this.predecessorStr)
        {
        	//check if the predecessor is in the activity list
        	for(Activity activity : activityList)
        	{
        		if(predecessor.equalsIgnoreCase(activity.activityName))
        			this.addPredecessor(activity);
        		
        	}
        }
	}
	
	public void LinkSuccessors(LinkedList<Activity> activityList){
		
		for(Activity act: activityList)
    	{
		
    		for(Activity actPred : act.predecessors)
    			if (this.activityName.equalsIgnoreCase(actPred.activityName))
    				this.addSuccessor(act);
    	}
	}
	//this method is to find the final activity
	public static Activity findLast(LinkedList<Activity> activityList)
	{
		for(Activity activity : activityList)
    	{
			if(activity.successors.isEmpty())
				return activity;
    	}
		return null;
	}
	
	//this method to find the first activity
	public static Activity findFirst(LinkedList<Activity> activityList)
	{
		for(Activity activity : activityList)
    	{
			if(activity.predecessors.isEmpty())
				return activity;
    	}
		return null;
	}
	
	public void addPredecessor(Activity v){
		this.predecessors.add(v);	
	}
	
	public void addSuccessor(Activity v){
		this.successors.add(v);	
	}
	public void addPredecessor(String pred)
	{
		this.addPredecessor(pred);
	}
	public boolean isRecStack() {
		return recStack;
	}

	public void setRecStack(boolean recStack) {
		this.recStack = recStack;
	}

	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public boolean isVisited() {
		return visited;
	}
	public void setVisited(boolean visited) {
		this.visited = visited;
	}

	public LinkedList<Activity> predecessors() {
		return predecessors;
	}

	public void setPredecessors(LinkedList<Activity> predecessors) {
		this.predecessors = predecessors;
	}
	
	//toString methods to print the activity name
	@Override
	public String toString(){
		return this.activityName;
	}

	//compareTo method - used in sorting activities
	@Override
    public int compareTo(Activity a)
    {
    	return this.activityName.compareTo(a.activityName);
    }
	
		
}


