/*
 * The goal of this program is to create an activity diagram based on user input.
 * The user will input all data for the network; activity, duration and dependencies.
 * Once all node are inserted we will have a full network diagram.
 * The user will then be able to receive data about the network of each possible path
 * to be taken and the corresponding duration.
 * 
 * Created By:
 * 				Omar Al Noman
 * 				Maxwell Buss
 * 				Alekhya Hari
 * 
 * Date: 10/12/18
 * 
 */

public class ActivityAnalyzer {
	
	
	public static void main(String[] args) {

		/* Create and display Activity Analyzer */
		
		ActivityAnalyzerGui AA = new ActivityAnalyzerGui();
		AA.setVisible(true);
		
		
          
	}
}
