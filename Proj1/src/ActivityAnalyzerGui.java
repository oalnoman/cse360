/*
 * This class is used to create the main GUI for this program and also process the user input
 * by calling the required methods
 */

import javax.swing.*;
import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Collections;
import java.util.LinkedList;

public class ActivityAnalyzerGui extends JFrame {
	
	// Variables declaration 
    private JTextField activityTextField;
    private JTextField durationTextField;
    private JLabel activityTitleLabel;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JLabel jLabel7;
    private JLabel jLabel8;
    private static JTextArea pathsTextArea;
    private JScrollPane jScrollPane1;
    private static JTextArea durationTextArea;
    private JScrollPane jScrollPane2;
    private JTextField predecessorsTextField;
    private JButton quitButton;
    private JButton resetButton;
    private JButton startProcessingButton;
    private JButton addActivityButton;
    
    private JMenuBar MenuBar;
    
    private JMenu fileMenu;
    private JMenu editMenu;
    private JMenu helpMenu;
    
    private JMenuItem createItem;
    private JMenuItem restartItem;
    private JMenuItem quitItem;

    private JMenuItem changeItem;

    private JMenuItem howItem;
    private JMenuItem aboutItem;
    
    private ButtonGroup buttonGroup;
    private static JRadioButton allPathsRadio;
    private static JRadioButton criticalPathsRadio;
    
    private int activityNum = 1; // a variable to keep track of the number of activities entered

    //LinkedList of the activities entered by the user
	static LinkedList<Activity> activityList = new LinkedList<>();
	private static final long serialVersionUID = 1L;
	
	
	//Default constructor
	public ActivityAnalyzerGui() {
        initComponents();
    }

  
     //This method is called from within the constructor to initialize the GUI.

    private void initComponents() {

        jLabel1 = new JLabel();
        jLabel2 = new JLabel();
        jLabel3 = new JLabel();
        jLabel4 = new JLabel();
        jLabel5 = new JLabel();
        jLabel6 = new JLabel();
        jLabel7 = new JLabel();
        jLabel8 = new JLabel();
        activityTitleLabel = new JLabel();
        
        activityTextField = new JTextField();
        durationTextField = new JTextField();
        predecessorsTextField = new JTextField();
        
        addActivityButton = new JButton();
        startProcessingButton = new JButton();
        quitButton = new JButton();
        resetButton = new JButton();
        
        pathsTextArea = new JTextArea();
        jScrollPane1 = new JScrollPane();
        durationTextArea = new JTextArea();
        jScrollPane2 = new JScrollPane();
       
        buttonGroup = new ButtonGroup();
        allPathsRadio = new JRadioButton();
        criticalPathsRadio = new JRadioButton();
        
        MenuBar = new JMenuBar();
        fileMenu = new JMenu();
        editMenu = new JMenu();
        helpMenu = new JMenu();
        
        createItem = new JMenuItem();
        restartItem = new JMenuItem();
        quitItem = new JMenuItem();
        
        changeItem = new JMenuItem();
        
        howItem = new JMenuItem();
        aboutItem = new JMenuItem();
        
        //default settings for the JFrame
        getContentPane().setBackground(Color.WHITE);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("Activity Analyzer v1.5");
        setResizable(false);
        
        //setup the components
        
        jLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 24)); 
        jLabel1.setText("Activity Analyzer");
        jLabel2.setText("Activity: ");
        jLabel3.setText("Duration: ");
        jLabel4.setText("Dependencies (Predecessors): ");
        jLabel5.setText("Output: ");
        jLabel6.setText("Enter a list of predecessors separated by comma");
        jLabel7.setText("Paths");
        jLabel8.setText("Duration");
        activityTitleLabel.setFont(new java.awt.Font("Lucida Grande", 0, 18));
        activityTitleLabel.setForeground(Color.RED);
        activityTitleLabel.setText("Activity " + activityNum);
        
        
        pathsTextArea.setText("");
        pathsTextArea.setEditable(false);
        pathsTextArea.setRows(5);
        pathsTextArea.setColumns(20);
        jScrollPane1.setViewportView(pathsTextArea);

        durationTextArea.setText("");
        durationTextArea.setColumns(1);
        durationTextArea.setRows(5);
        durationTextArea.setEditable(false);
        jScrollPane2.setViewportView(durationTextArea);
        
        
        addActivityButton.setText("Add Activity");
        addActivityButton.addActionListener( event -> addActivity() );
        startProcessingButton.setText("Start Processing");
        startProcessingButton.addActionListener( event -> startProcessing() );        
        quitButton.setText("Quit");
        quitButton.addActionListener(event -> quit());       
        resetButton.setText("Restart");
        resetButton.addActionListener(event -> reset());
        
        
        buttonGroup.add(allPathsRadio);
        allPathsRadio.setText("Print all paths");
        allPathsRadio.setSelected(true);
        buttonGroup.add(criticalPathsRadio);
        criticalPathsRadio.setText("Print critical path(s)");
        
        allPathsRadio.addActionListener(event -> selection());
        criticalPathsRadio.addActionListener(event -> selection());

        fileMenu.setText("File");
        createItem.setText("Create a report");
        createItem.addActionListener(event -> createReport());
        
        fileMenu.add(createItem);
        
        restartItem.setText("Restart");
        restartItem.addActionListener(event -> reset());
        fileMenu.add(restartItem);
        
        quitItem.setText("Quit");
        quitItem.addActionListener(event -> quit());
        fileMenu.add(quitItem);
        
        MenuBar.add(fileMenu);

        editMenu.setText("Edit");
        changeItem.setText("Change duration");
        changeItem.addActionListener(event -> updateDuration());
        
        changeItem.setEnabled(false);
        editMenu.add(changeItem);
        MenuBar.add(editMenu);

        helpMenu.setText("Help");
        howItem.setText("How to use the software");
        howItem.addActionListener(event -> help());
        helpMenu.add(howItem);
        
        aboutItem.setText("About");
        aboutItem.addActionListener(event -> about());
        helpMenu.add(aboutItem);
        
        MenuBar.add(helpMenu);

        setJMenuBar(MenuBar);
        

        
        //check if duration is integer
        durationTextField.addFocusListener(new FocusListener(){
			@Override
			public void focusGained(FocusEvent e) {
			}

			@Override
			public void focusLost(FocusEvent e) {
			
				JTextField durationTest = (JTextField)e.getComponent();
				
				if(durationTest.getText().isEmpty())
					durationTest.setText("0");
				else
				{
					try{
						Integer.parseInt(durationTest.getText());
			        }catch(NumberFormatException exp)
			        {
			        	
			        	JOptionPane.showMessageDialog(null,
			        		    "Duration must be integer!",
			        		    "Error",
			        		    JOptionPane.ERROR_MESSAGE);
			        	durationTest.setText("");
			        	durationTest.requestFocusInWindow();
			        }
				}
	
			}
        });
        
        
        //check if more than one activity is entered at a time
        activityTextField.addFocusListener(new FocusListener(){
			@Override
			public void focusGained(FocusEvent e) {
				
				}

			@Override
			public void focusLost(FocusEvent e) {
				
				JTextField activityNameTest = (JTextField)e.getComponent();
				if(activityNameTest.getText().contains(","))
				{
					JOptionPane.showMessageDialog(null,
	        		    "You can only add one activity at a time!",
	        		    "Error",
	        		    JOptionPane.ERROR_MESSAGE);
					activityNameTest.setText("");
					activityNameTest.requestFocusInWindow();
				
		        }
			}
	
		});
       



        //Group Layout         
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(208, 208, 208)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel5)
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addGap(435, 435, 435))
                                .addGroup(layout.createSequentialGroup()
                                        .addComponent(allPathsRadio)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(criticalPathsRadio))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(resetButton)
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(quitButton)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(6, 6, 6)
                                        .addComponent(jLabel6))
                                    .addComponent(predecessorsTextField)
                                    .addComponent(durationTextField)
                                    .addComponent(activityTextField)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 399, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jScrollPane2, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel8)
                                .addGap(30, 30, 30)))))
                .addContainerGap(42, Short.MAX_VALUE))
            .addGroup(GroupLayout.Alignment.CENTER, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                		.addComponent(activityTitleLabel)
                        .addGap(100, 100, 100)
                        .addContainerGap(100, 100))
                .addGroup(GroupLayout.Alignment.TRAILING,layout.createSequentialGroup() 
                        .addComponent(addActivityButton)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(startProcessingButton)
                        .addGap(90, 90, 90))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(activityTitleLabel)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(activityTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(durationTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(predecessorsTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(allPathsRadio)
                        .addComponent(criticalPathsRadio))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(startProcessingButton)
                    .addComponent(addActivityButton))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5))
             
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane2)
                    .addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 51, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(quitButton)
                    .addComponent(resetButton))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null); //to center the window in the middle of the screen
        
    }

    
    //addActivity - This methods is used to parse the user input, and create activity
    private void addActivity() {
    	
    	String activityName;
        int duration = 0;
        String[] predecessors;
        LinkedList<String> predLink = new LinkedList<String>();

        //get the data and parse it
        activityName = this.activityTextField.getText();
        
        //check if the activity name is left blank
        if(activityName.isEmpty())
        {
        	JOptionPane.showMessageDialog(null,
        		    "Error: Activity Name should not be blank! ",
        		    "Error",
        		    JOptionPane.ERROR_MESSAGE);
        	activityTextField.requestFocusInWindow();
        }
        else
        {	
	        duration = Integer.parseInt(this.durationTextField.getText());
	       
	        predecessors = this.predecessorsTextField.getText().split("\\s*,\\s*");
	        for(String pred : predecessors)
	        {
	        	predLink.add(pred);
	        }
	        
	        //create the activity
	        activityList.add(new Activity(activityName,duration, new LinkedList<String>(predLink)));
	        
	        
	        
	        //clear the fields
	        activityTextField.setText("");
	        durationTextField.setText("");
	        predecessorsTextField.setText("");
	        
	        //set focus of the activity field
	        activityTextField.requestFocusInWindow();
	        
	        activityNum++; //increase the counter for the activity
	        
	        activityTitleLabel.setText("Activity " + activityNum);
        }
    }
    
    // a method used to link the dependencies to the related activity
    private void linkNeighbors()
    {
    	for(Activity activity: activityList)
    	{
    		
    		activity.LinkPredecessors(activityList);
    	}
    	
    	for(Activity activity: activityList)
    	{
    		activity.LinkSuccessors(activityList);
    	}
    	
    }
    
    //startProcessing - this method is used to check if the list is empty, check for cycles,
    //check if all nodes are connected, create paths, sort paths, and output the paths with durations
    private void startProcessing() {
    	
    	//link predecessors to the activity
    	linkNeighbors();
    	
    	//check if the Activity List is empty
    	if(activityList.isEmpty())
    		JOptionPane.showMessageDialog(null,
        		    "Error: There is no activity to process! ",
        		    "Error",
        		    JOptionPane.ERROR_MESSAGE);
    	
    	else
    	{
    	//Error Checking
		
			//check if the activities have a cycle
			if(ErrorChecking.isCyclic(activityList))
			{
				JOptionPane.showMessageDialog(null,
	        		    "A cycle was Detected! click on Reset to start over or Quit!",
	        		    "Error",
	        		    JOptionPane.ERROR_MESSAGE);
			}
			
			else if(!(ErrorChecking.isConnected(activityList)))
			{
				JOptionPane.showMessageDialog(null,
	        		    "Not all nodes are connected! click on Reset to start over or Quit! ",
	        		    "Error",
	        		    JOptionPane.ERROR_MESSAGE);
			}
			
			else
			{	
				// No Errors
				//reset the visited and reStack to false
				ErrorChecking.resetVisitedandreStack(activityList);
				
				//enable change duration option
				changeItem.setEnabled(true);
				
				// create Paths , Sort them, Print them
				
				Path.createPaths(activityList); //create the paths
				
				Collections.sort(Path.paths); // sort the path
				Collections.reverse(Path.paths); // reverse the order
				
				printPaths();
				
			}
    	
    	}
    	
    	//if finished processing or exit out because of an error 
    	// erase the fields and disable them so the user is forced to restart the program
    	activityTextField.setText("");
    	durationTextField.setText("");
    	predecessorsTextField.setText("");
    	
    	activityTitleLabel.setText(" Processing is done! Click on Edit to change durations");
    	
    	activityTextField.setEnabled(false);
    	durationTextField.setEnabled(false);
    	predecessorsTextField.setEnabled(false);
    	
    	addActivityButton.setEnabled(false);
        startProcessingButton.setEnabled(false);
    }
    
    // a function to show the paths in the GUI
    public static void printPaths()
    {
    	String pathsStr = "";
		String durationStr = "";
		
    	//print the paths and their durations
		if(allPathsRadio.isSelected())
		{
			for(Path p : Path.paths)
			{
				pathsStr += p.toString() + "\n";
				durationStr += p.totalDuration + "\n";
			}
		}
		//print the critical paths and their duration (only critical paths!!!)
		if(criticalPathsRadio.isSelected())
		{
			//find the critical paths
			for(Path criticalP : Path.criticalPaths())
			{
				pathsStr += criticalP.toString() + "\n";
				durationStr += criticalP.totalDuration + "\n";
			}
			
		}
		pathsTextArea.setText(pathsStr);
		durationTextArea.setText(durationStr);
    }
    
    //create the report
    private void createReport()
    {
    	String pathsOutput = "";
    	String activityOutput = "";
    	
    	for(Path p : Path.paths)
		{
			pathsOutput += String.format("%-50s %d \n" ,p.toString(), p.totalDuration);
		}
    	
    	Collections.sort(activityList); //sort the activities in alphaNumerical order
    	
    	for(Activity a : activityList)
    	{
    		activityOutput += a.toString() + "\n";
    	}
    	
    	
    	String file = JOptionPane.showInputDialog("Please enter a name for the report!");
    	Date now = new Date();

    	BufferedWriter BF;
    	
    	try {
			BF = new BufferedWriter(new FileWriter(file + ".txt"));
			BF.write(String.format("\n%40s\n","Activity Analyzer Report"));
			BF.write("---------------------------------------------------------- \n");
			BF.write("The report created on: " + now + "\n");
			BF.write("---------------------------------------------------------- \n\n");
			BF.write("Activity List: \n\n");
			BF.write(activityOutput + "\n");
			BF.write(String.format("%-48s%s \n","Path", "Duration")); //add the paths here
			BF.write(pathsOutput);
			BF.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    	JOptionPane.showMessageDialog(null, "The report was created successfully!","Create a report" ,JOptionPane.INFORMATION_MESSAGE);
    	
    }
    
    //this function is called when the user changes the selection of the radio button
    //after the paths has been processed
    private void selection()
    {
    	if(!(pathsTextArea.getText().isEmpty()))
    	{
    		printPaths();
    	}
    }
    
    //Display Update Duration Window
    private void updateDuration()
    {
    	new UpdateDurationGui().setVisible(true);
    }
    
    private void quit(){
    	System.exit(0); //terminate successfully 
    }
    
    private void reset(){
    	
    	//clear activity list
    	activityList.clear();
    	//clear the created paths
    	Path.paths.clear();
    	
    	
    	//set the visibility of the text fields
    	activityTextField.setEnabled(true);
    	durationTextField.setEnabled(true);
    	predecessorsTextField.setEnabled(true);
    	
    	//enable the commands buttons
    	addActivityButton.setEnabled(true);
        startProcessingButton.setEnabled(true);
    	
    	//clear the text fields
        durationTextField.setText("");
        predecessorsTextField.setText("");
        pathsTextArea.setText("");
        durationTextArea.setText("");
        activityTextField.setText("");
        
        //set the focus on activity name field
        activityTextField.requestFocusInWindow();
        
        //Reset the visibility of the change duration option
        changeItem.setEnabled(false);
         
        activityNum = 1; //reset the activity counter
        activityTitleLabel.setText("Activity " + activityNum);
    }    
    
    //shows information about the program and the team
    private void about(){
    	
    	 new About().setVisible(true);
    }
    
    //display a help message
    private void help(){
    	
    	JOptionPane.showMessageDialog(null,
    		    "To properly use \"Activity Analyzer\", follow the steps below: \n"
    		    + "\n 1. Type the activity name in the first text box"
    		    + "\n\t\t (you can only add one activity at a time!). \n "
    		    + "\n\n 2. Specify the duration for the activity in the second box. "
    		    + "\n \t\t (duration must be integer, don't use decimals). \n"
    		    + "\n\n 3. The third text is for the activity predecessors."
    		    + "\n\n 4. The \"add activity\"  button is used to add activity to the diagram. \n"
    		    + "\n\n 5. Once you added all the activities, click on \"start processing\" button "
    		    + "\n \t\t to show all the possible paths for the activities provided."
    		    + "\n\n 6. After the activities have been processed, you can change the duration"
    		    + "\n \t\t of each activity by clicking on edit from the menu and choose"
    		    + "\n \t\t Change duration. \n\n\n",
    		    "Help!",
    		    JOptionPane.INFORMATION_MESSAGE);
    	
    }
}
