/*
 * This class is used to check for what is considered an error based on the project requirements
 * which is (check if the diagram is cyclic and check if all nodes are connected) 
 */

import java.util.LinkedList;

public class ErrorChecking {

	// Returns true if the graph contains a  
    // cycle, else false.  
    public static boolean isCyclic(LinkedList<Activity> activityList)  
    { 

        // Call the recursive helper function
        for (Activity a : activityList) 
            if (isCyclicHelper(a))
                return true; 
  
        return false; 
    } 
    

   private static boolean isCyclicHelper(Activity i)  
   { 
         
       // Mark the current node as visited and 
       // part of recursion stack 
       if (i.isRecStack()) 
           return true; 
 
       if (i.isVisited()) 
           return false; 
             
       i.setVisited(true);
 
       i.setRecStack(true);
         
       for (Activity a: i.predecessors) 
           if (isCyclicHelper(a))
               return true; 
                 
       i.setRecStack(false); 
 
       return false; 
   } 
 

   //check if all nodes are connected by traversing all the connected nodes
   //then check if one of the nodes in the activity list is not visited then it is not connected
   public static boolean isConnected(LinkedList<Activity> activityList)
   {
	   LinkedList<Activity> copy = new LinkedList<Activity>(activityList);
	   copy.remove(Activity.findFirst(activityList));
	   
	   for (Activity a : copy)
	   {
		   if(a.predecessors.isEmpty())
		   {
			   return false;
		   }
	   }
	   
	   
	   //visit all nodes
	   visitNodes(activityList.getLast());
	  
	   for (Activity a : activityList)
	   {
		   //check if there is a node not visited
		   //if there is such node then not all nodes are connected
		   if(!a.isVisited())
			   return false;
	   }
	   return true;
   }
   
   //Helper method for the isConnected function
   private static void visitNodes(Activity activity)
   {
	   if(activity.isVisited())
		   return;
	   activity.setVisited(true);
	   for(Activity a : activity.predecessors)
	   {
		 if (!a.isVisited()) 
			 visitNodes(a);
	   }
	   
	   
   }
   
   //reset the visited and RecStack variables to false.
   public static void resetVisitedandreStack(LinkedList<Activity> activityList)
   {
	   for(Activity a : activityList)
	   {
		   a.setVisited(false);
		   a.setRecStack(false);
	   }
   }
    
    

}
