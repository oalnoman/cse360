/*
 * This class is used to create the path which is consists of LinkedList of activities
 * and the total duration of the path
 */
import java.util.Collections;
import java.util.LinkedList;

public class Path implements Comparable<Path>
{
	
	//Variables
	
	//Linked List of all paths created - so we can sort them later
	static LinkedList<Path> paths = new LinkedList<Path>();
	
	LinkedList<Activity> path = new LinkedList<Activity>();
	int totalDuration;
	
	//constructor
	public Path(LinkedList<Activity> path, int duration)
	{
		this.path = path;
		this.totalDuration = duration;
	}
	
	//compareTo method which is used in the sorting
	@Override
    public int compareTo(Path p)
    {
    	return Integer.valueOf(this.totalDuration).compareTo(p.totalDuration);
    }
    
	//crticalPaths - a function that return only critical paths of the activity diagram
	public static LinkedList<Path> criticalPaths()
	{
		LinkedList<Path> criticalPaths = new LinkedList<Path>();
		
		Collections.sort(Path.paths); // sort the path
		Collections.reverse(Path.paths); // reverse the order
		
		int criticalDuration = paths.getFirst().totalDuration; //first path has the max duration
		//iterate throw the paths and check if there is more than one critical path
		for(Path path : paths)
		{
			if(path.totalDuration < criticalDuration)
				break;
			criticalPaths.add(path);
		}
		
		return criticalPaths;
	}
	//toString to print out the path as a string
    @Override
    public String toString(){
    	int size = this.path.size()-1;
    	String pathStr = "";
    	for(int i = 0; i < size; i++)
    	{
    		pathStr +=this.path.get(i).toString() + " -> ";
    	}
    	pathStr += this.path.getLast().toString();
    	return pathStr;
    }
    
    //generate all possible paths in the activity diagram and store it in LinkedList of paths
  	 public static void createPaths(LinkedList <Activity> activity)  
  	    { 
  	        LinkedList<Activity> pathList = new LinkedList<>(); 
  	        Activity last = Activity.findLast(activity);
  	        Activity first = Activity.findFirst(activity);
  	       
  	        //add beginning of the path  
  	        pathList.add(last); 
  	          
  	        //Call recursive helper function from last node to first node
  	        					//source			//destination		//path
  	        createPathsHelper(last, first, pathList); 
  	        
  	    } 
  	  
  	    // A recursive function to generate the paths 
  	    // all paths from 'u' to 'd'. 
  	    // localPathList<> stores the 
  	    // activities in the current path 
  	    private static void createPathsHelper(Activity s, Activity d, 
  	                            LinkedList<Activity> localPathList) { 
  	          
  	        // Mark the current node 
  	        s.setVisited(true);
  	          
  	        if (s.equals(d))  
  	        { 
  	        	//when we finished generating the path
  	        	//find its total duration
  	        	int totalDuration = 0;
  	        	for(Activity i : localPathList)
  	        	{
  	        		totalDuration += i.duration;
  	        	}
  	        	
  	        	//create path class with localPathList and totalDuration
  	        	//add the path to the LinkedList of paths
  	        	paths.addFirst(new Path(new LinkedList<Activity>(localPathList), totalDuration));

  	        } 
  	          
  	        // Recurse for all the activities that are 
  	        // predecessors to the current activity 
  	        for (Activity i : s.predecessors)  
  	        { 
  	            if (!i.isVisited()) 
  	            { 
  	                // store current node  
  	                // in path
  	                localPathList.addFirst(i); 
  	                createPathsHelper(i, d, localPathList); 
  	                 //Here we are done creating the path
  	                //we should back track to generate the other paths
  	                // remove current node 
  	                // in path 
  	                localPathList.remove(i); 
  	            } 
  	        } 
  	          
  	        // Mark the current node 
  	        s.setVisited(false);
  	    }  
  
}
