import java.awt.Color;
import java.util.Collections;

import javax.swing.*;

public class UpdateDurationGui extends JFrame {
	
	// Variables declaration 
    private JComboBox<Activity> activityListCombo;
    private JButton apply;
    private JButton close;
    private JTextField currentDuration;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JTextField newDuration;
    private static final long serialVersionUID = 1L;
    
    //constructor
    public UpdateDurationGui() {
        initComponents();
    }

   //Initialize components
    private void initComponents() {

        jLabel1 = new JLabel();
        activityListCombo = new JComboBox<>();
        jLabel2 = new JLabel();
        jLabel3 = new JLabel();
        currentDuration = new JTextField();
        jLabel4 = new JLabel();
        newDuration = new JTextField();
        apply = new JButton();
        close = new JButton();

        //set default settings for the JFrame
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        getContentPane().setBackground(Color.WHITE);
        setTitle("Edit Activity Duration");
        setResizable(false);
        

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 0, 18));
        jLabel1.setText("Edit Activity Duration");

        activityListCombo.setModel(new DefaultComboBoxModel<>());
        for (Activity activity : ActivityAnalyzerGui.activityList)
        {
        	activityListCombo.addItem(activity);
        
        }
        activityListCombo.setSelectedIndex(-1);
        activityListCombo.addActionListener(event -> setCurrent());
        

        jLabel2.setText("Activity: ");

        jLabel3.setText("Current Duration:");

        currentDuration.setEnabled(false);

        jLabel4.setText("New Duration:");

        apply.setText("Apply");
        apply.addActionListener(event -> update());

        close.setText("Close");
        close.addActionListener(event -> close());

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(99, 99, 99)
                .addComponent(jLabel1)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 155, GroupLayout.PREFERRED_SIZE)
                        .addComponent(close)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(apply))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 80, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                            .addComponent(activityListCombo, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(currentDuration)
                            .addComponent(newDuration, GroupLayout.PREFERRED_SIZE, 183, GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jLabel1)
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(activityListCombo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(currentDuration, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(newDuration, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(apply)
                    .addComponent(close))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        pack();
      //Display the window in the middle of the screen
        setLocationRelativeTo(null);
    }
    
    private void close(){
    	dispose();
    }
    
    private void update(){
    	int newD = Integer.parseInt(newDuration.getText());
    	int oldD = Integer.parseInt(currentDuration.getText());
    	int durationDiff = newD - oldD;
    	
    	//update the activity
    	int index = activityListCombo.getSelectedIndex();
    	ActivityAnalyzerGui.activityList.get(index).duration += durationDiff;
    	
    	//update the value in the GUI textbox
    	setCurrent();
    	newDuration.setText("");
    	//update the paths that has the activity
    	for(Path p : Path.paths)
    	{
    		if(p.path.contains(ActivityAnalyzerGui.activityList.get(index)))
    		{
    			p.totalDuration += durationDiff;
    		}
    	}
    	
    	//resort the paths
    	Collections.sort(Path.paths); // sort the path
		Collections.reverse(Path.paths); // reverse the order
		
		//print the paths
		ActivityAnalyzerGui.printPaths();
    	
    }
    
    private void setCurrent(){
    	int index = activityListCombo.getSelectedIndex();
    	currentDuration.setText(Integer.toString(ActivityAnalyzerGui.activityList.get(index).duration));
    }
    
}
